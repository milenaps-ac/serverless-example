'use strict';

const logger = require('./api/logger');

exports.handler = (event, context, callback) => {

	logger.createLogger();

	logger.info('Hello World!');
	logger.info('Ops, neither of this will come up in cloudwatch if the lambda log_level is not explicitly set to info (warn is default).');

	logger.warn('Testing %s.. 1, 2, 3.', 'winston');

	callback(null, 'Successful execution');
};