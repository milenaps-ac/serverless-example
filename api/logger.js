'use strict';

const winston = require('winston');

let logger;

exports.createLogger = () => {
  logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({
        level: process.env.log_level || 'warn',
        timestamp: true
      })
    ]
  });
};

exports.debug = (message, ...args) => {
  logger.log('debug', message, ...args);
};

exports.info = (message, ...args) => {
  logger.log('info', message, ...args);
};

exports.warn = (message, ...args) => {
  logger.log('warn', message, ...args);
};

exports.error = (message, ...args) => {
  logger.log('error', message, ...args);
};